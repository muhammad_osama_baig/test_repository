/* @author Copyright RIKSOF (Private) Limited.
    This jenkins pipeline is used for the configuration of ubuntu local machines.
    Following must be configured before running pipeline.
    1. Create keys on jenkins machine


*/

pipeline {
    agent any 
    parameters {
        string( name: 'IP', defaultValue: '', description: 'Enter laptop IP address' )
        string( name: 'username', defaultValue: '', description: 'Enter laptop username by typing: whoami' )
        string( name: 'employee_username', defaultValue: '', description: 'Enter laptop employee_username' )
        string( name: 'employee_full_name', defaultValue: '', description: 'Enter employee_full_name' )
        string( name: 'pemfile', defaultValue: '', description: 'Enter laptop content of pem file' )
        booleanParam( name: 'SSH_authorization', defaultValue: false, description: 'Authorize jenkins to local machine' )
        booleanParam( name: 'chrome', defaultValue: false, description: 'Install chrome' )
        booleanParam( name: 'libreoffice', defaultValue: false, description: 'Install libreoffice')
        booleanParam( name: 'python', defaultValue: false, description: 'Install python')
        booleanParam( name: 'Visual_Studio_Code', defaultValue: false, description: 'Install Visual Studio Code' )
        booleanParam( name: 'atom', defaultValue: false, description: 'Install Atom' )
        booleanParam( name: 'nodejs', defaultValue: false, description: 'Install nodejs' )
        booleanParam( name: 'build_essential', defaultValue: false, description: 'Install build_essential' )
        booleanParam( name: 'android_studio', defaultValue: false, description: 'Install android_studio' )
        booleanParam( name: 'Gradle', defaultValue: false, description: 'Install Gradle' )
        booleanParam( name: 'Zoom', defaultValue: false, description: 'Install Zoom' )
        booleanParam( name: 'git_lfs', defaultValue: false, description: 'Install git_lfs' )
        booleanParam( name: 'skype', defaultValue: false, description: 'Install skype' )
        booleanParam( name: 'slack', defaultValue: false, description: 'Install slack' )
        booleanParam( name: 'NVM', defaultValue: false, description: 'Install NVM' )
        booleanParam( name: 'Anydesk', defaultValue: false, description: 'Install Anydesk' )
        booleanParam( name: 'Robo_3T', defaultValue: false, description: 'Install Robo_3T' )
    }
    stages {
        stage ('SSH_authorization') {
            when {
               expression {
          return params.SSH_authorization
          }
        }
            steps {
                script{
                              
                    sh 'sudo touch id.txt'
                    sh 'sudo chown jenkins:jenkins id.txt'
                    sh "echo ${params.pemfile} > id.txt"
                    sh 'sudo base64 --decode id.txt > id'
                    sh 'sudo rm -rf id.txt'
                    sh 'sudo mv id ~/id.pem'
                    sh 'sudo chmod 400 ~/id.pem'
                    sh 'sudo cp ~/.ssh/id_rsa.pub ~/'
                    sh 'sudo mv ~/id_rsa.pub ~/authorized_keys'
                    sh 'sudo chown jenkins:jenkins ~/authorized_keys'
                    sh "scp -i ~/id.pem ~/authorized_keys ${params.username}@${params.IP}:/home/${params.username}/.ssh/"
                    
                    }
                }
           }
        stage ('Install Chrome') {
            when {
               expression {
          return params.chrome
          }
        }
            steps {
                script{
                    remote = [:]
                    remote.name = params.username
                    remote.host = params.IP
                    remote.allowAnyHosts = true
                    remote.failOnError = true
                    withCredentials([sshUserPrivateKey(credentialsId: 'jenkins-id-key', keyFileVariable: 'identity', passphraseVariable: '', usernameVariable: params.username)]) {
                    remote.user = params.username
                    remote.identityFile = identity

                    def cmd = "wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb; "
                    cmd += 'sudo apt install ./google-chrome-stable_current_amd64.deb -y; '
                    
                    sshCommand remote: remote, command: cmd
                    
                    }
                }
           }
        }
        stage ('Install libreoffice') {
            when {
            expression {
          return params.libreoffice
          }
        }
            steps {
                script{
                    remote = [:]
                    remote.name = params.username
                    remote.host = params.IP
                    remote.allowAnyHosts = true
                    remote.failOnError = true
                    withCredentials([sshUserPrivateKey(credentialsId: 'jenkins-id-key', keyFileVariable: 'identity', passphraseVariable: '', usernameVariable: params.username)]) {
                    remote.user = params.username
                    remote.identityFile = identity
                    
                    def cmd = "sudo apt update -y; "
                    cmd += 'sudo apt install libreoffice -y; '
                    
                    sshCommand remote: remote, command: cmd
                    
                    }
                }
           }
        }
        stage ('Install python') {
            when {
               expression {
          return params.python
          }
        }
            steps {
                script{
                    remote = [:]
                    remote.name = params.username
                    remote.host = params.IP
                    remote.allowAnyHosts = true
                    remote.failOnError = true
                    withCredentials([sshUserPrivateKey(credentialsId: 'jenkins-id-key', keyFileVariable: 'identity', passphraseVariable: '', usernameVariable: params.username)]) {
                    remote.user = params.username
                    remote.identityFile = identity
                    
                    def cmd = "sudo apt install python2 -y; "
                    cmd += 'sudo update-alternatives --install "/usr/bin/python" "python" "/usr/bin/python2" 1; '
                    
                    sshCommand remote: remote, command: cmd
                    
                    }
                }
           }
        }
        stage ('Install Visual_Studio_Code') {
            when {
               expression {
          return params.Visual_Studio_Code
          }
        }
            steps {
                script{
                    remote = [:]
                    remote.name = params.username
                    remote.host = params.IP
                    remote.allowAnyHosts = true
                    remote.failOnError = true
                    withCredentials([sshUserPrivateKey(credentialsId: 'jenkins-id-key', keyFileVariable: 'identity', passphraseVariable: '', usernameVariable: params.username)]) {
                    remote.user = params.username
                    remote.identityFile = identity
                    
                    sshCommand remote: remote, command: "sudo snap install code --classic"
                    
                    }
                }
           }
        }
        stage ('Install atom') {
            when {
               expression {
          return params.atom
          }
        }
            steps {
                script{
                    remote = [:]
                    remote.name = params.username
                    remote.host = params.IP
                    remote.allowAnyHosts = true
                    remote.failOnError = true
                    withCredentials([sshUserPrivateKey(credentialsId: 'jenkins-id-key', keyFileVariable: 'identity', passphraseVariable: '', usernameVariable: params.username)]) {
                    remote.user = params.username
                    remote.identityFile = identity
                    
                    sshCommand remote: remote, command: "sudo snap install atom --classic"
                    
                    }
                }
           }
        }
        stage ('Install nodejs') {
            when {
               expression {
          return params.nodejs
          }
        }
            steps {
                script{
                    remote = [:]
                    remote.name = params.username
                    remote.host = params.IP
                    remote.allowAnyHosts = true
                    remote.failOnError = true
                    withCredentials([sshUserPrivateKey(credentialsId: 'jenkins-id-key', keyFileVariable: 'identity', passphraseVariable: '', usernameVariable: params.username)]) {
                    remote.user = params.username
                    remote.identityFile = identity
                    
                    def cmd = "curl -sL https://deb.nodesource.com/setup_14.x -o nodesource_setup.sh; "
                    cmd += 'sudo bash nodesource_setup.sh; '
                    cmd += 'sudo apt install nodejs -y; '
                    
                    sshCommand remote: remote, command: cmd
                    
                    }
                }
           }
        }
        stage ('Install build_essential') {
            when {
               expression {
          return params.build_essential
          }
        }
            steps {
                script{
                    remote = [:]
                    remote.name = params.username
                    remote.host = params.IP
                    remote.allowAnyHosts = true
                    remote.failOnError = true
                    withCredentials([sshUserPrivateKey(credentialsId: 'jenkins-id-key', keyFileVariable: 'identity', passphraseVariable: '', usernameVariable: params.username)]) {
                    remote.user = params.username
                    remote.identityFile = identity
                    
                    sshCommand remote: remote, command: "sudo apt install build-essential -y"
                    
                    }
                }
           }
        }
        stage ('Install android_studio') {
            when {
               expression {
          return params.android_studio
          }
        }
            steps {
                script{
                    remote = [:]
                    remote.name = params.username
                    remote.host = params.IP
                    remote.allowAnyHosts = true
                    remote.failOnError = true
                    withCredentials([sshUserPrivateKey(credentialsId: 'jenkins-id-key', keyFileVariable: 'identity', passphraseVariable: '', usernameVariable: params.username)]) {
                    remote.user = params.username
                    remote.identityFile = identity
                    
                    sshCommand remote: remote, command: "sudo snap install android-studio --classic"
                    
                    }
                }
           }
        }
        stage ('Install Gradle') {
            when {
               expression {
          return params.Gradle
          }
        }
            steps {
                script{
                    remote = [:]
                    remote.name = params.username
                    remote.host = params.IP
                    remote.allowAnyHosts = true
                    remote.failOnError = true
                    withCredentials([sshUserPrivateKey(credentialsId: 'jenkins-id-key', keyFileVariable: 'identity', passphraseVariable: '', usernameVariable: params.username)]) {
                    remote.user = params.username
                    remote.identityFile = identity
                    
                    def cmd = "cd /opt; "
                    cmd += 'sudo wget https://services.gradle.org/distributions/gradle-4.10.3-all.zip; '
                    cmd += 'sudo unzip gradle*.zip; '
                    cmd += 'sudo mv gradle-4.10.3 gradle; '
                    cmd += 'sudo update-alternatives --install "/usr/bin/gradle" "gradle" "/opt/gradle/bin/gradle" 1; '
                    
                    sshCommand remote: remote, command: cmd
                    
                    }
                }
           }
        }
        stage ('git_lfs') {
            when {
               expression {
          return params.git_lfs
          }
        }
            steps {
                script{
                    remote = [:]
                    remote.name = params.username
                    remote.host = params.IP
                    remote.allowAnyHosts = true
                    remote.failOnError = true
                    withCredentials([sshUserPrivateKey(credentialsId: 'jenkins-id-key', keyFileVariable: 'identity', passphraseVariable: '', usernameVariable: params.username)]) {
                    remote.user = params.username
                    remote.identityFile = identity
                    
                    sshCommand remote: remote, command: "sudo apt-get install git-lfs -y"
                    
                    }
                }
           }
        }
        stage ('skype') {
            when {
               expression {
          return params.skype
          }
        }
            steps {
                script{
                    remote = [:]
                    remote.name = params.username
                    remote.host = params.IP
                    remote.allowAnyHosts = true
                    remote.failOnError = true
                    withCredentials([sshUserPrivateKey(credentialsId: 'jenkins-id-key', keyFileVariable: 'identity', passphraseVariable: '', usernameVariable: params.username)]) {
                    remote.user = params.username
                    remote.identityFile = identity
                    
                    sshCommand remote: remote, command: "sudo snap install skype --classic"
                    
                    }
                }
           }
        }
        stage ('slack') {
            when {
               expression {
          return params.slack
          }
        }
            steps {
                script{
                    remote = [:]
                    remote.name = params.username
                    remote.host = params.IP
                    remote.allowAnyHosts = true
                    remote.failOnError = true
                    withCredentials([sshUserPrivateKey(credentialsId: 'jenkins-id-key', keyFileVariable: 'identity', passphraseVariable: '', usernameVariable: params.username)]) {
                    remote.user = params.username
                    remote.identityFile = identity
                    
                    sshCommand remote: remote, command: "sudo snap install slack --classic"
                    
                    }
                }
           }
        }
        stage ('Install NVM') {
            when {
               expression {
          return params.NVM
          }
        }
            steps {
                script{
                    remote = [:]
                    remote.name = params.username
                    remote.host = params.IP
                    remote.allowAnyHosts = true
                    remote.failOnError = true
                    withCredentials([sshUserPrivateKey(credentialsId: 'jenkins-id-key', keyFileVariable: 'identity', passphraseVariable: '', usernameVariable: params.username)]) {
                    remote.user = params.username
                    remote.identityFile = identity
                    
                    def cmd = "curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.37.2/install.sh | bash; "
                    cmd += 'export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"; '
                    //cmd += '[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm"; '
                    
                    sshCommand remote: remote, command: cmd
                    
                    }
                }
           }
        }
        stage ('Install Robo_3T') {
            when {
               expression {
          return params.Robo_3T
          }
        }
            steps {
                script{
                    remote = [:]
                    remote.name = params.username
                    remote.host = params.IP
                    remote.allowAnyHosts = true
                    remote.failOnError = true
                    withCredentials([sshUserPrivateKey(credentialsId: 'jenkins-id-key', keyFileVariable: 'identity', passphraseVariable: '', usernameVariable: params.username)]) {
                    remote.user = params.username
                    remote.identityFile = identity
                    
                    def cmd = "sudo apt update -y; "
                    cmd += 'sudo apt install snapd -y; '
                    cmd += 'sudo snap install robo3t-snap; '
                    
                    sshCommand remote: remote, command: cmd
                    
                    }
                }
           }
        }
        stage ('Install Anydesk') {
            when {
               expression {
          return params.Anydesk
          }
        }
            steps {
                script{
                    remote = [:]
                    remote.name = params.username
                    remote.host = params.IP
                    remote.allowAnyHosts = true
                    remote.failOnError = true
                    withCredentials([sshUserPrivateKey(credentialsId: 'jenkins-id-key', keyFileVariable: 'identity', passphraseVariable: '', usernameVariable: params.username)]) {
                    remote.user = params.username
                    remote.identityFile = identity
                    
                    def cmd = "sudo apt update -y; "
                    cmd += 'sudo apt -y upgrade; '
                    cmd += 'wget -qO - https://keys.anydesk.com/repos/DEB-GPG-KEY | sudo apt-key add -; '
                    cmd += 'echo "deb http://deb.anydesk.com/ all main" | sudo tee /etc/apt/sources.list.d/anydesk-stable.list; '
                    cmd += 'sudo apt update -y; '
                    cmd += 'sudo apt install anydesk -y; '
                    
                    sshCommand remote: remote, command: cmd
                  }   
                }
              }
           }
           stage ('username and permissions') {
            when {
               expression {
          return params.employee_username
          }
        }
            steps {
                script{
                    remote = [:]
                    remote.name = params.username
                    remote.host = params.IP
                    remote.allowAnyHosts = true
                    remote.failOnError = true
                    withCredentials([sshUserPrivateKey(credentialsId: 'jenkins-id-key', keyFileVariable: 'identity', passphraseVariable: '', usernameVariable: params.username)]) {
                    remote.user = params.username
                    remote.identityFile = identity
                    
                    def cmd = "sudo adduser ${params.employee_username} --gecos '${params.employee_full_name},RoomNumber,WorkPhone,HomePhone' --disabled-password; "
                    cmd += "sudo echo '${params.employee_username}:Riksof' | sudo chpasswd"
                    //cmd += "sudo usermod -aG docker,employee ${params.employee_username}; "
                    
                    sshCommand remote: remote, command: cmd
                    
                    }
                }
           }
        }
    }
}